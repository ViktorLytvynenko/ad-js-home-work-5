let usersLink = "https://ajax.test-danit.com/api/json/users";
let postsLink = "https://ajax.test-danit.com/api/json/posts";
let users;
let posts;
let loader = document.querySelector("#loader")
localStorage.setItem('modal', 'closed')

class Card {
    constructor(name, email, title, body, footer, footer2, footer3, id) {
        this.name = name;
        this.email = email;
        this.title = title;
        this.body = body;
        this.footer = footer;
        this.footer2 = footer2;
        this.footer3 = footer3;
        this.id = id;
    }
}


let names = () => {
    users.forEach(user => {
        console.log(user.name);
    })
}
const innerPosts = () => {
    let ulElement = document.querySelector("#ulElement");
    ulElement.innerHTML = ""
    posts.forEach((post, index) => {
        let postItem = new Card(null, null, null, null, "share", "edit", "save", post.id);
        postItem.title = post.title;
        postItem.body = post.body;

        users.forEach((user, index) => {
            if (user.id === post.userId) {
                postItem.name = user.name;
                postItem.email = user.email;
            }
        })
        ulElement.innerHTML += `
            <li id="liElement${index}">
            <div class="container" id="post-${post.id}">
            <span class="top">${postItem.name}</span>
            <span class="top-email">${postItem.email}</span>
            <button id="button" class="button">Delete</button>
            </div>
            <p class="title">${postItem.title}</p>
            <p id="body-${post.id}" class="body">${postItem.body}</p>
            <a href="#" class="footer">${postItem.footer}</a>
            <a href="#" id="edit-${post.id}" class="footer2">${postItem.footer2}</a>
            <a href="#" id="save-${post.id}" class="footer3">${postItem.footer3}</a>
            </li>`;  
    })
    let editBtns = document.querySelectorAll('.footer2');
    editBtns.forEach((btnEdit)=>{
        btnEdit.addEventListener('click',(e) => {
            let currentId = btnEdit.id.substring(5)

            let currentBody = document.querySelector(`#body-${currentId}`)
            let textOnPost = currentBody.innerText
            currentBody.innerHTML = `
                <textarea id="inputNewText-${currentId}" placeholder="Edit post">${textOnPost}</textarea>
            `

            let saveBtn = document.querySelector(`#save-${currentId}`)
            saveBtn.style.display = "inline-block"
            btnEdit.style.display = "none"
        })
    })
    let saveBtns = document.querySelectorAll('.footer3')
    saveBtns.forEach((saveBtn) => {
        saveBtn.addEventListener('click',(e) => { 
            let currentId = saveBtn.id.substring(5)
            let inputNewText = document.querySelector(`#inputNewText-${currentId}`).value
            fetch(`${postsLink}/${currentId}`,{
                method: "PUT",
                body: JSON.stringify({
                    body:inputNewText 
                })
            }).then(res => res.json()).then(data => {
                let currentBody = document.querySelector(`#body-${currentId}`)
                currentBody.innerHTML = inputNewText

                let editBtn = document.querySelector(`#edit-${currentId}`)
                editBtn.style.display = "inline-block"
                saveBtn.style.display = "none"
            })
        })
    })
    let deleteButtons = document.querySelectorAll("#button");
    deleteButtons.forEach(deleteButton => {
        deleteButton.addEventListener("click", (event) => {
            let idEl = parseInt(event.target.parentNode.id.substring(5));
            fetch(`https://ajax.test-danit.com/api/json/posts/${idEl}`, {
                method: "DELETE"
            }).then((res) => {
                    posts = posts.filter((post) => {
                        if (idEl !== post.id) {
                            return post;
                        }
                    })
                    innerPosts()
            })

        })
    })
}
let generation = () => {
    setTimeout(loader => {
        document.body.innerHTML = `
        <div class="h1">Latest news</div>
        <div class="modalWindow">
            <label for="title">Write new title</label>
            <input type="text" name="title" id="title">
             <label for="main">Write text</label>
             <textarea rows="5" cols="30" name="main" id="main"></textarea>
             <button class="saveButton" id="saveButton">Save</button>
        </div>
        <button class="loadMore" id="loadMore">Add new publication</button>
        <ul id='ulElement' class='ulElement'></ul>`;
        let loadMoreButton = document.querySelector("#loadMore");
        let modalWindow = document.querySelector(".modalWindow");
        let saveButton = document.querySelector("#saveButton");
        let modalForm = {
            title: document.querySelector("#title"),
            main: document.querySelector("#main")
        }
        loadMoreButton.addEventListener("click", () => {
            if (localStorage.getItem('modal') === 'closed'){
                localStorage.setItem('modal', 'opened')
                modalWindow.style.display = "block"
                loadMoreButton.innerText = "Close modal window"
            } else {
                localStorage.setItem('modal', 'closed')
                modalWindow.style.display = "none"
                loadMoreButton.innerText = "Add new publication "
            }
        })
        saveButton.addEventListener("click", () => {
            fetch(postsLink, {
                method: "POST",
                body: JSON.stringify({
                    userId: 1,
                    title: modalForm.title.value,
                    body: modalForm.main.value
                }),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(response => response.json())
                .then(json =>{
                    posts = [
                        {
                            ...json
                        },
                        ...posts
                    ]
                    innerPosts()
                    localStorage.setItem('modal', 'closed')
                    modalWindow.style.display = "none"
                    loadMoreButton.innerText = "Add new publication "
                })
        })

        innerPosts()
    }, 2000);


}


fetch(usersLink).then((res) => res.json()).then((data) => {
    users = [...data];
    names(users);
    fetch(postsLink).then((res) => res.json()).then((data) => {

        posts = [...data];
       generation(users);
    });
});

